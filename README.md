# Thesis Template
[![pipeline status](https://git.rwth-aachen.de/khalil.baydoun/MS-khalil-baydoun/badges/master/pipeline.svg)](https://git.rwth-aachen.de/khalil.baydoun/ms-khalil-baydoun/commits/master)

## Download Thesis
[![latest proposal](https://img.shields.io/badge/Download-Proposal-blue.svg)](https://git.rwth-aachen.de/khalil.baydoun/ms-khalil-baydoun/-/jobs/artifacts/master/raw/proposal/text/proposal.pdf?job=build-proposal-text)
[![latest proposal slides](https://img.shields.io/badge/Download-Proposal%20Presentation-blue.svg)](https://git.rwth-aachen.de/khalil.baydoun/ms-khalil-baydoun/-/jobs/artifacts/master/raw/proposal/presentation/proposal-slides.pdf?job=build-proposal-slides)

Replace `thesis-template` with your Gitlab Project name.
Update from `proposal` to `thesis` once you reached this stage. 


Also update the [.gitlab-ci.yml](./.gitlab-ci.yml#L11) and change `proposal`to `thesis`

## Configuration
Adjust the [config.tex](preamble/config.tex). 
