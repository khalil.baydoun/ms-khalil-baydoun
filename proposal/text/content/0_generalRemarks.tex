\paragraph{Purpose of Proposal:}
An orientation phase precedes the thesis. This phase is intended to be used for:
\begin{compactitem}
	\item reviewing related work (literature),
	\item becoming acquainted with the environment conditions for the implementation,
	\item clearly defining the problem which has to be tackled in the thesis. 
\end{compactitem}

The orientation phase results in an abstract ("proposal") which is about 15 (Bachelor) to 20 (Master) pages in length and has to include:
\begin{compactitem}
	\item the well-defined problem to be solved and goals to be achieved,
	\item a comparative study of first ideas for solution esp. with regard to related work (literature),
	\item a short presentation of the chosen approach, and
	\item a well calculated working plan. 
\end{compactitem}
Additional details can be found in the corresponding sections of this document.

This proposal is discussed with the tutors (professor and assistant) and therefore has to be presented in form of a so called proposal presentation (duration approx. 30 min) at the I5 seminar at the end of the orientation phase. It is seen as a kind of a "contract" both sides can refer to. This is to avoid misunderstandings concerning the topic of the diploma thesis and a serious misjudgement of the work load (resulting in an excessive duration of the thesis). Consequently a proposal is also enforced for external diploma theses where Prof. Decker or Prof. Jarke is only second reviewer.

\paragraph{Proposal Expectations:}
Introduction, Related Work, Concept and detailled Project Plan (consisting of clearly defined, concrete task descriptions / deliverables with associated workload estimations and deadlines for all work to be done - including evaluation and writing time - as well as buffer time) to be completed before the thesis is registered. There should also be bullet points outlining how you might approach implementation and evaluation.

Note that you can directly reuse the results of your proposal for the final thesis.

\paragraph{General Remarks on Writing a Thesis:}
It is absolutely essential, that a thesis tells a consistent story with a \textbf{common thread} (``Roter Faden'')! Skipping from one topic to another without a clear common theme, thread or other framing that ensures the reader is guided along the common thread must be avoided.

A good practice for writing a thesis is working iteratively: 
\begin{compactenum}
	\item Think about the story (common thread) your thesis should be telling.
	\item Outline the concrete things you want to touch upon with bullet points.
	\item Formulate key content for each point. Mark sections that need further work with todos (e.g. using the \href{https://www.ctan.org/pkg/todonotes}{\texttt{todonotes}} package) and return to them at a later time.
	\item Provide proper reasoning and/or sources (i.e. \texttt{\textbackslash{}cite\{\dots\}}) for all statements that are not clearly your own opinion.
	\item Connect the content to create a consisistent story, following the common thread definded in 1.
\end{compactenum}

Similarly, when structuring sections, make sure to start with a short (1-3 sentences) structured summary of the topic covered in the section and end with a similarly short summary of the key finding and how they lead to the next section. These paragraphs especially help to reestablish the common thread when the content of individual sub(sub)sections is not clearly related.

The written elaboration is \textbf{NOT} a documentation of the created programs. It shows the student’s ability to apply or develop computer science methods autonomously. Results taken from literature have to be marked carefully to be distinguishable from the student's own work. Programming notations like for example "if a=b then s1" must be formatted different from descriptive texts and other notations (e.g. mathematical expressions or using a Latex \href{https://de.overleaf.com/learn/latex/Algorithms}{algorithm package}) to avoid misunderstandings. The thesis has to be written in English or German. A mixture of languages must be avoided.
Also try to 
\begin{compactitem}
	\item Employ consistent tenses, punctuation, grammar, colorschemes and iconography. 
	\item Employ figures to support comprehensibility and shorten your textual descriptions.
	\item Use typography, listings, enumerations, itemizations and math mode to help structure your text.
	\item Employ formalization and abstraction to clearly separate concept and implementation.
\end{compactitem}

\paragraph{Thesis structure:}
The work on the diploma thesis follows the detailed working plan established during the orientation phase: at first the chosen approach has to be detailed. This step is then followed by the implementation of the approach, its analysis, and documentation. Eventually the results have to be written down. The assessment of the thesis is based not only on the accurateness of the implementation and written elaboration but also on the student's autonomy regarding defining the topic, reviewing literature, and finding solutions. Additionally a discerning review of design decisions and alternatives (from the computer science point of view and possibly also from the application specific point of view) affects the assessment. For a practical thesis the following basic outline for the written elaboration can be used as a starting point (while it does in no way claim to be complete):

\begin{compactenum}
\item Start with context and derive a concrete problem statement (What problem do you want to solve? What is the concrete task of the thisis?). 

\item Outline what has already been tried (related work), why that is insufficient and how you will tackle that problem (differently), concretizing your research question (What is the novelty? How is your research problem different from that of the related work?).  

\item Derive a (formal) model of the solution (concept section - a concept is an abstract solution for the formulated problem, fulfilling functional requirements. This part must \textbf{NOT} contain any implementation details! )

\item Describe the implementation/realization of that concept (i.e. how you actually implemented the abstract solution concept in code, the libraries and tools you used etc).

\item Evaluation the performance of your solution with respect to the initially defined goals and requirements (both functional and non-functional).

\item Summarize what you have done and how you showed that it solves the problem you previously defined. What is the impact of those findings (i.e. what do the results mean to the field of computer sciene)? Finally describe where your work might be extended further or what related research questions might have come up along the way.
\end{compactenum}